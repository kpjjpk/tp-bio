# TP Bioinformática
===================

## Como compilar
`./gradlew shadowJar`

## Como ejecutar ejercicios
- Ejercicio 1
  `./ex1.sh`
- Ejercicio 2
  `./ex2.sh`
- Ejercicio 3
  `./ex3.sh` <archivo de secuencia> <regex, formato Java>
- Ejercicio 4
  `./ex4.sh`

**NOTA**
Ejecutar todos los comandos en el directorio raíz

## Resultados
- Ejercicio 1
  Genera un archivo `AK313707.fas` en el directorio actual con todas las secuencias
- Ejercicio 2
  Genera archivos `AK313707-SEQUENCE_i.out`, con `i` entre 0 y 5; uno por cada reading frame, en orden:
  1, 2 y 3 (hacia adelante) y 1, 2 y 3 (reversos)
- Ejercicio 3
  Genera un archivo con sufijo `-hits.txt`, el cual contiene la lista de hits que matchearan con la expresión regular
- Ejercicio 4
  Genera un archivo `.orf` con los ORFs y todos los transcriptos, junto con archivos `.patmatmotifs` (uno para cada transcripto) indicando morfologías similares) y, finalmente, el resultado en el archivo `results.txt`
