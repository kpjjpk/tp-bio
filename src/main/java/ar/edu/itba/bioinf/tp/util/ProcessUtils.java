package ar.edu.itba.bioinf.tp.util;

public class ProcessUtils {
    public static void retryOnInterruptions(Interruptible process) {
        while (true) {
            try {
                process.run();
                return;
            } catch (InterruptedException exc) {
                // intentional
            }
        }
    }

    @FunctionalInterface
    public interface Interruptible {
        void run() throws InterruptedException;
    }
}
