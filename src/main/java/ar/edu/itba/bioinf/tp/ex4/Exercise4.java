package ar.edu.itba.bioinf.tp.ex4;

import static ar.edu.itba.bioinf.tp.util.ParamUtils.checkArgCount;

import org.apache.commons.io.FilenameUtils;

public class Exercise4 {
    private static final int EXPECTED_ARGC = 2;
    private static final String ORF_EXTENSION = ".orf";

    public static void main(String[] args) throws Exception {
        checkArgCount(args, EXPECTED_ARGC);
        String inputFilePath = args[0];

        String firstOutputFilePath = FilenameUtils.getBaseName(inputFilePath)
                + ORF_EXTENSION;

        EMBOSSORFFinder orfFinder = new EMBOSSORFFinder();
        orfFinder.findORFs(inputFilePath, firstOutputFilePath);

        String lastOutputFilePath = args[1];
        EMBOSSProteinDomainAnalyzer proteinDomainAnalyzer = new EMBOSSProteinDomainAnalyzer();
        proteinDomainAnalyzer.analyzeProteins(firstOutputFilePath,
                lastOutputFilePath);
    }
}
