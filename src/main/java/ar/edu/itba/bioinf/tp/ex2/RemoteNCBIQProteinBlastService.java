package ar.edu.itba.bioinf.tp.ex2;

import static org.biojava.nbio.ws.alignment.qblast.BlastAlignmentParameterEnum.ENTREZ_QUERY;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.biojava.nbio.core.sequence.ProteinSequence;
import org.biojava.nbio.core.sequence.io.FastaReaderHelper;
import org.biojava.nbio.ws.alignment.qblast.BlastOutputFormatEnum;
import org.biojava.nbio.ws.alignment.qblast.BlastProgramEnum;
import org.biojava.nbio.ws.alignment.qblast.NCBIQBlastAlignmentProperties;
import org.biojava.nbio.ws.alignment.qblast.NCBIQBlastOutputProperties;
import org.biojava.nbio.ws.alignment.qblast.NCBIQBlastService;

public class RemoteNCBIQProteinBlastService {
    private static final String BLAST_EXTENSION = ".out";

    public void runBlast(String inputFilePath, String blastDb,
            String entrezQuery) throws Exception {

        Map<String, ProteinSequence> proteinSequencesByKey = readProteinSequences(
                inputFilePath);

        NCBIQBlastAlignmentProperties alignmentProperties = new NCBIQBlastAlignmentProperties();
        alignmentProperties.setBlastProgram(BlastProgramEnum.blastp);
        alignmentProperties.setBlastDatabase(blastDb);
        alignmentProperties.setAlignmentOption(ENTREZ_QUERY, entrezQuery);

        NCBIQBlastOutputProperties outputProperties = new NCBIQBlastOutputProperties();
        outputProperties.setOutputFormat(BlastOutputFormatEnum.Text);

        String inputFileBaseName = FilenameUtils.getBaseName(inputFilePath);

        for (Entry<String, ProteinSequence> entry : proteinSequencesByKey
                .entrySet()) {
            ProteinSequence proteinSequence = entry.getValue();

            String outputFilePath = inputFileBaseName + "-" + entry.getKey()
                    + BLAST_EXTENSION;
            runBlast(outputFilePath, alignmentProperties, outputProperties,
                    proteinSequence);
        }
    }

    private void runBlast(String outputFilePath,
            NCBIQBlastAlignmentProperties alignmentProperties,
            NCBIQBlastOutputProperties outputProperties,
            ProteinSequence proteinSequence) throws Exception {
        NCBIQBlastService service = new NCBIQBlastService();

        String requestId = null;
        try {
            String stringSequence = proteinSequence.getSequenceAsString();
            requestId = service.sendAlignmentRequest(stringSequence,
                    alignmentProperties);

            waitForResults(service, requestId);

            InputStream is = service.getAlignmentResults(requestId,
                    outputProperties);

            writeResultsToFile(is, outputFilePath);
        } finally {
            service.sendDeleteRequest(requestId);
        }
    }

    private Map<String, ProteinSequence> readProteinSequences(String path)
            throws IOException {
        File inputFile = new File(path);
        Map<String, ProteinSequence> proteinSequences = FastaReaderHelper
                .readFastaProteinSequence(inputFile);

        for (Entry<String, ProteinSequence> entry : proteinSequences
                .entrySet()) {
            System.out.println(entry.getValue().getOriginalHeader() + "="
                    + entry.getValue().getSequenceAsString());
        }

        return proteinSequences;
    }

    private void waitForResults(NCBIQBlastService service, String requestId)
            throws InterruptedException, Exception {
        while (!service.isReady(requestId)) {
            System.out.println("Waiting for results. Sleeping for 5 seconds");
            Thread.sleep(5000);
        }
    }

    private void writeResultsToFile(InputStream is, String outputFilePath)
            throws IOException, Exception {
        File outputFile = new File(outputFilePath);

        System.out.println(
                "Saving query results in file: " + outputFile.getAbsolutePath());

        try (InputStreamReader isr = new InputStreamReader(is);
                BufferedReader reader = new BufferedReader(isr);
                FileWriter writer = new FileWriter(outputFile)) {

            IOUtils.copy(is, writer);
        }
    }
}
