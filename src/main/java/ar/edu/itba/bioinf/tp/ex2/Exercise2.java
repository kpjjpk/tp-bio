package ar.edu.itba.bioinf.tp.ex2;

import static ar.edu.itba.bioinf.tp.util.ParamUtils.checkArgCount;

public class Exercise2 {
    private static final int EXPECTED_ARGC = 1;
    private static final String BLAST_DB = "swissprot";
    private static final String ENTREZ_QUERY = "mammals[Organism]";

    public static void main(String[] args) throws Exception {
        checkArgCount(args, EXPECTED_ARGC);

        RemoteNCBIQProteinBlastService proteinBlastService = new RemoteNCBIQProteinBlastService();
        proteinBlastService.runBlast(args[0], BLAST_DB, ENTREZ_QUERY);
    }
}
