package ar.edu.itba.bioinf.tp.util;

public class ParamUtils {
    public static <T> void checkArgCount(T[] args, int expected) {
        if (args.length != expected) {
            throw new IllegalArgumentException(
                    "Wrong number of arguments (expected: " + expected
                            + ", got: " + args.length + ")");
        }
    }
}
