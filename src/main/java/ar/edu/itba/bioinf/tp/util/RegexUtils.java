package ar.edu.itba.bioinf.tp.util;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexUtils {
    public static List<String> getMatches(String regex, String haystack) {
        Pattern pattern = Pattern.compile(regex);
        return getMatches(pattern, haystack);
    }

    public static List<String> getMatches(Pattern pattern, String haystack) {
        Matcher matcher = pattern.matcher(haystack);

        List<String> hits = new ArrayList<String>();
        while (matcher.find()) {
            String hit = matcher.group();
            hits.add(hit);
        }

        return hits;
    }

    public static String getMatch(Pattern pattern, String haystack) {
        return getMatches(pattern, haystack).get(0);
    }
}
