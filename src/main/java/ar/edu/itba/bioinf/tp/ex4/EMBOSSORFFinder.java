package ar.edu.itba.bioinf.tp.ex4;

import java.io.IOException;

import org.apache.commons.io.IOUtils;

import static ar.edu.itba.bioinf.tp.util.ProcessUtils.retryOnInterruptions;

public class EMBOSSORFFinder {
    private static final String EMBOSS_CLI_COMMAND = "getorf";
    private static final String OUTPUT_REDIRECT_PARAM = "outseq";

    public void findORFs(String inputFilePath, String outputFilePath)
            throws IOException {
        Runtime runtime = Runtime.getRuntime();

        Process process = runtime.exec(EMBOSS_CLI_COMMAND + " " + inputFilePath
                + " -" + OUTPUT_REDIRECT_PARAM + " " + outputFilePath);
        IOUtils.copy(process.getErrorStream(), System.out);

        retryOnInterruptions(() -> process.waitFor());
    }

}
