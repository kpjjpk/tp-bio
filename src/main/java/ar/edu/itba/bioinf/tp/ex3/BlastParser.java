package ar.edu.itba.bioinf.tp.ex3;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;

import ar.edu.itba.bioinf.tp.util.RegexUtils;

public class BlastParser {
    private static final String HIT_REGEX_LEFT = ">[^>BLAST]+[\\|][^>]*";
    private static final String HIT_REGEX_RIGHT = "[^>]*";

    public List<String> parseHits(String inputFilePath, String regex)
            throws FileNotFoundException, IOException {
        String fileContents = FileUtils
                .readFileToString(new File(inputFilePath));

        return RegexUtils.getMatches(HIT_REGEX_LEFT + regex + HIT_REGEX_RIGHT,
                fileContents);
    }
}
