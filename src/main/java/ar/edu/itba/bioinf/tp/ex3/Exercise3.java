package ar.edu.itba.bioinf.tp.ex3;

import static ar.edu.itba.bioinf.tp.util.ParamUtils.checkArgCount;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;

public class Exercise3 {
    private static final int EXPECTED_ARGC = 2;

    public static void main(String[] args) throws Exception {
        checkArgCount(args, EXPECTED_ARGC);
        String inputFilePath = args[0];
        String regex = args[1];

        BlastParser blastParser = new BlastParser();
        List<String> hits = blastParser.parseHits(inputFilePath, regex);

        if (hits.isEmpty()) {
            System.out.println("Results: No hits found");
            return;
        }

        System.out.println("Results: " + hits.size() + " hits");
        String outputFilePath = FilenameUtils.getBaseName(inputFilePath)
                + "-hits.txt";

        try (OutputStream os = new FileOutputStream(outputFilePath)) {
            for (String hit : hits) {
                System.out.println(hit);
                IOUtils.write(hit + "\n", os);
            }
        }
    }
}
