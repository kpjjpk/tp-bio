package ar.edu.itba.bioinf.tp.ex4;

import static ar.edu.itba.bioinf.tp.util.ProcessUtils.retryOnInterruptions;

import java.io.File;
import java.io.IOException;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import ar.edu.itba.bioinf.tp.util.RegexUtils;

public class EMBOSSProteinDomainAnalyzer {
    // XXX disclaimer: I'm not particularly proud of anything below

    private static final String EMBOSS_CLI_COMMAND = "patmatmotifs";
    private static final String OUTPUT_REDIRECT_PARAM = "outfile";
    private static final String EXTENSION = ".patmatmotifs";

    private static final String NO_HITS_STRING = "HitCount: 0";

    private static final String PROTEIN_SEQUENCES_REGEX = ">[^>]+";
    private static final String PROTEIN_HEADER_REGEX = ">[^>\\[\\]]+";
    private static final String PROTEIN_BODY_PATTERN = "][\\s]?[A-Z\\*]+";

    private static final Pattern proteinHeaderPattern = Pattern
            .compile(PROTEIN_HEADER_REGEX);
    private static final Pattern proteinBodyPattern = Pattern
            .compile(PROTEIN_BODY_PATTERN);

    public void analyzeProteins(String inputFilePath, String outputFilePath)
            throws IOException {
        List<Entry<String, String>> proteinSequences = readProteinSequences(
                inputFilePath);

        if (proteinSequences.isEmpty()) {
            System.out.println("No protein sequences found");
            return;
        } else {
            System.out.println("Analyzing " + proteinSequences.size()
                    + " protein sequences..");
        }

        List<Entry<String, String>> proteinsWithHits = analyzeProteins(
                proteinSequences);

        writeResults(outputFilePath, proteinsWithHits);
    }

    private List<Entry<String, String>> readProteinSequences(
            String inputFilePath) throws IOException {
        String fileContents = FileUtils
                .readFileToString(new File(inputFilePath));

        List<String> rawEntries = RegexUtils.getMatches(PROTEIN_SEQUENCES_REGEX,
                fileContents);

        List<Entry<String, String>> proteinSequences = new ArrayList<>(
                rawEntries.size());
        for (String rawEntry : rawEntries) {
            String header = RegexUtils.getMatch(proteinHeaderPattern, rawEntry)
                    .replaceAll("[a-z]*\\|", "").replace(">", "").trim();
            String body = RegexUtils.getMatch(proteinBodyPattern, rawEntry)
                    .replaceAll("[\\s\\]]", "");

            proteinSequences.add(new SimpleEntry<String, String>(header, body));
        }
        return proteinSequences;
    }

    private List<Entry<String, String>> analyzeProteins(
            List<Entry<String, String>> proteinSequences) throws IOException {
        Runtime runtime = Runtime.getRuntime();

        List<Entry<String, String>> proteinsWithHits = new ArrayList<>();
        for (Entry<String, String> proteinSequence : proteinSequences) {
            System.out.println(
                    "Analyzing sequence: " + proteinSequence.getKey() + "..");

            if (analyzeProtein(runtime, proteinSequence)) {
                proteinsWithHits.add(proteinSequence);
            }
        }
        return proteinsWithHits;
    }

    private boolean analyzeProtein(Runtime runtime,
            Entry<String, String> proteinSequence) throws IOException {
        // XXX patmatmotifs doesn't accept sequences from stdin
        File tempFile = File.createTempFile(proteinSequence.getKey(), ".tmp");
        FileUtils.write(tempFile, proteinSequence.getValue());

        String outputFilePath = proteinSequence.getKey() + EXTENSION;

        Process process = runtime.exec(
                EMBOSS_CLI_COMMAND + " -full " + tempFile.getAbsolutePath()
                        + " -" + OUTPUT_REDIRECT_PARAM + " " + outputFilePath);
        IOUtils.copy(process.getErrorStream(), System.out);

        retryOnInterruptions(() -> process.waitFor());

        String analysis = FileUtils.readFileToString(new File(outputFilePath));
        return !analysis.contains(NO_HITS_STRING);
    }

    private void writeResults(String outputFilePath,
            List<Entry<String, String>> proteinsWithHits) throws IOException {
        String rawResultsHeader = "Sequences with matches:";
        String resultsSeparator = StringUtils.repeat("=",
                rawResultsHeader.length());
        String resultsHeader = rawResultsHeader + "\n" + resultsSeparator;

        String resultsTable = resultsHeader + "\n"
                + proteinsWithHits.stream().map(entry -> "# " + entry.getKey())
                        .collect(Collectors.joining("\n"));

        String resultsDetail = "";
        for (Entry<String, String> proteinWithHits : proteinsWithHits) {
            resultsDetail += FileUtils.readFileToString(
                    new File(proteinWithHits.getKey() + EXTENSION)) + "\n";
        }

        String rawDetailsHeader = "Details:";
        String detailsSeparator = StringUtils.repeat("=",
                rawDetailsHeader.length());
        String details = rawDetailsHeader + "\n" + detailsSeparator + "\n"
                + resultsDetail;

        String results = resultsTable + "\n\n" + details;

        System.out.println("\n" + results);
        FileUtils.write(new File(outputFilePath), results);
    }
}
