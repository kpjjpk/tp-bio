package ar.edu.itba.bioinf.tp.ex1;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.AbstractMap.SimpleEntry;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.biojava.nbio.core.exceptions.CompoundNotFoundException;
import org.biojava.nbio.core.sequence.AccessionID;
import org.biojava.nbio.core.sequence.DNASequence;
import org.biojava.nbio.core.sequence.ProteinSequence;
import org.biojava.nbio.core.sequence.RNASequence;
import org.biojava.nbio.core.sequence.compound.DNACompoundSet;
import org.biojava.nbio.core.sequence.compound.NucleotideCompound;
import org.biojava.nbio.core.sequence.io.DNASequenceCreator;
import org.biojava.nbio.core.sequence.io.FastaWriterHelper;
import org.biojava.nbio.core.sequence.io.GenbankReader;
import org.biojava.nbio.core.sequence.io.GenericGenbankHeaderParser;
import org.biojava.nbio.core.sequence.transcription.Frame;

public class NucleotideSequenceTranslator {
    private static final String FASTA_EXTENSION = ".fas";

    public void translateAll(String path) throws Exception {
        File inputFile = new File(path);

        Map<String, DNASequence> nucleotideSequencesByKey = readNucleotideSequences(
                inputFile);

        for (Entry<String, DNASequence> entry : nucleotideSequencesByKey
                .entrySet()) {
            DNASequence nucleotideSequence = entry.getValue();
            List<ProteinSequence> transcripts = getAllTranscripts(
                    nucleotideSequence);

            File outputFile = new File(entry.getKey() + FASTA_EXTENSION);
            FastaWriterHelper.writeProteinSequence(outputFile, transcripts);
        }
    }

    private Map<String, DNASequence> readNucleotideSequences(File file)
            throws FileNotFoundException, IOException,
            CompoundNotFoundException {
        // TODO GenbankReaderHelper.readGenbankDNASequence(file)
        try (FileInputStream fis = new FileInputStream(file)) {
            GenbankReader<DNASequence, NucleotideCompound> dnaReader = new GenbankReader<>(
                    fis, new GenericGenbankHeaderParser<>(),
                    new DNASequenceCreator(DNACompoundSet.getDNACompoundSet()));
            return dnaReader.process();
        }
    }

    private List<ProteinSequence> getAllTranscripts(
            DNASequence nucleotideSequence) {
        return getAllTranscriptsByFrame(nucleotideSequence).map(Entry::getValue)
                .collect(Collectors.toList());
    }

    private Stream<Entry<Frame, ProteinSequence>> getAllTranscriptsByFrame(
            DNASequence nucleotideSequence) {
        final AccessionIDGenerator idGenerator = new AccessionIDGenerator();

        return Arrays.stream(Frame.getAllFrames()).map(frame -> {
            RNASequence rnaSequence = nucleotideSequence.getRNASequence(frame);
            ProteinSequence proteinSequence = rnaSequence.getProteinSequence();
            proteinSequence.setAccession(idGenerator.getAccessionID());
            return new SimpleEntry<>(frame, proteinSequence);
        });
    }

    private static class AccessionIDGenerator {
        static final String BASE_ID = "SEQUENCE_";

        int i = 0;

        AccessionID getAccessionID() {
            String id = BASE_ID + String.valueOf(i);
            i++;
            return new AccessionID(id);
        }
    }
}
