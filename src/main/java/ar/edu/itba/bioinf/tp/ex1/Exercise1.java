package ar.edu.itba.bioinf.tp.ex1;

import static ar.edu.itba.bioinf.tp.util.ParamUtils.checkArgCount;

public class Exercise1 {
    private static final int EXPECTED_ARGC = 1;

    public static void main(String[] args) throws Exception {
        checkArgCount(args, EXPECTED_ARGC);

        NucleotideSequenceTranslator translator = new NucleotideSequenceTranslator();
        translator.translateAll(args[0]);
    }
}
